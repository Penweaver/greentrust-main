@extends('layout')
@section('content')
    <!-- header area start -->
    <section class="header-area ">
        <div class="header-inner" id="header-carousel">
            @foreach($sliders as $data)
                <div class="single-slider-item"
                     style="background-size:cover;background-position:center;background-image: url(assets/images/slider/{{$data->image}});">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2">
                                <h1 class="title text-center">{!! $data->title !!}</h1>
                                <p>{!! $data->description !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    <!-- header area end -->

    <!-- about area start -->
    <section class="about-us-aera about-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="left-content-area">
                        <div class="thumb">
                            <img src="{{asset('assets/images/about-video-image.jpg')}}" alt="about left image">
                            <div class="hover">
                                <a href="{!! $basic->	about_video !!}" class="mfp-iframe video-play-btn">
                                    <i class="fas fa-play"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="right-content-area">
                        {!! $basic->about !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- about area end -->

    <!-- counter and subscribe area start -->
    <section class="counter-and-subscribe-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-rm-6">
                    <div class="single-counter-box"><!-- single counter box -->
                        <div class="bg-icon"><i class="pe-7s-users"></i></div>
                        <div class="counter-number">
                            <span class="count-numb">{{$totalUsers}}</span>
                        </div>
                        <h4 class="name">Users</h4>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-rm-6">
                    <div class="single-counter-box rrmt-30">
                        <!-- single counter box -->
                        <div class="bg-icon two"><i class="pe-7s-news-paper"></i></div>
                        <div class="counter-number">
                            <span class="count-numb">{{$totalblogs}}</span>
                        </div>
                        <h4 class="name">Blog</h4>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-rm-6">
                    <div class="single-counter-box rmt-30">
                        <!-- single counter box -->
                        <div class="bg-icon three"><i class="pe-7s-like2"></i></div>
                        <div class="counter-number">
                            <span class="count-numb">{{$totalSubscriber}}</span>
                        </div>
                        <h4 class="name">Subscribers</h4>
                    </div>
                    <!-- //. single counter box -->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="subscribe-outer-wrapper"><!-- subscribe form wrapper -->
                        <h2 class="title">Subscribe for <strong>more updates</strong></h2>
                        <div class="subscribe-form-wrapper">
                            <form action="{{route('subscribe')}}" method="post">
                                @csrf
                                <div class="form-element">
                                    <input name="email" type="email" placeholder="Enter your email address" class="input-field">
                                </div>
                                <input type="submit" value="subscribe" class="submit-btn">
                            </form>
                        </div>
                    </div><!-- subscribe form wrapper -->
                </div>
            </div>
        </div>
    </section>
    <!-- counter and subscribe area end -->

    <!-- news feed area start -->
    <section class="news-feed-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2">
                    <div class="section-title text-center">
                        <h2 class="title">news
                            <strong> feeds</strong>
                        </h2>
                        <div class="separator ">
                            <img src="{{asset('assets/images/logo/favicon.png')}}" alt="image">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($posts as $k=>$post)
                    @php
                        $catSlug =  str_slug($post->category->name);
                        $slug  = str_slug($post->title);
                    @endphp
                    <div class="col-lg-4 col-md-4 col-sm-6 col-rm-6">
                        <div class="single-new-feed-item rmb-30">
                            <div class="thumb">
                                <img src="{{asset('assets/images/post/'.$post->image)}}" alt="{{$post->title}}">

                            </div>
                            <div class="content">
                                <a href="{{route('cats.blog',[$post->category->id,$catSlug])}}">
                                <span class="subtitle">{{$post->category->name}}</span>
                                </a>
                                <a href="{{route('blog.details',[$post->id,$slug])}}"><h4 class="title">When an unknown printer took a galley of
                                        type.</h4>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- news feed area end -->
    @include('partials.get-contact')
    @include('partials.clients')




@stop

@section('script')

@stop