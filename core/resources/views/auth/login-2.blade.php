<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>GreenTrust Login</title>
    <link rel="icon" href="{{asset('assets-new/assets/images/favicon.png')}}" type="image/png" sizes="16x16">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Label Multipurpose Startup Business Template">
    <meta name="keywords" content="Label HTML Template, Label Startup Business Template, Startup Template">
    <link href="{{asset('assets-new/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600%7COpen+Sans:400%7CVarela+Round" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets-new/css/animate.css')}}"> <!-- Resource style -->
    <link rel="stylesheet" href="{{asset('assets-new/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('assets-new/css/owl.theme.css')}}">
    <link rel="stylesheet" href="{{asset('assets-new/css/jquery.accordion.css')}}">
    <link rel="stylesheet" href="{{asset('assets-new/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('assets-new/css/ionicons.min.css')}}"> <!-- Resource style -->
    <link href="{{asset('assets-new/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{asset('css/access.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href='http://fonts.googleapis.com/css?family=Rokkitt' rel='stylesheet' type='text/css'>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
  </head>
  <body>
    <div class="wrapper">
      <div class="container">
        <nav class="navbar navbar-expand-md navbar-light nav-white bg-light fixed-top">
          <div class="container">
            <a class="navbar-brand" href="{{route('homepage')}}">GreenTrust</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav ml-auto navbar-right">
                  <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{route('homepage')}}">Home</a></li>
				  <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{route('register')}}">Sign Up</a></li>
                </ul>
            </div>
          </div>
        </nav>
      </div>


      <div id="main" class="main">
        <div class="cta-big">
        <div class="wrap">
		<!-- strat-contact-form -->	
		<div class="contact-form">
		<!-- start-form -->
	
		<form class="contact_form" action="{{ route('login') }}" method="post" name="contact_form">
		     {{csrf_field()}}
		<h1>Login Into Your Account</h1>
	    <ul>
	        <li>
	            <!--<input type="email" class="textbox1" name="email" placeholder="email address" required />-->
	            <input type="text" class="textbox1 @if($errors->has('username')) error  @endif" name="username" placeholder="Username" required />
	             @if ($errors->has('username'))
                                            <span class="error ">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                        @endif
	            <span class="form_hint">Enter a valid Username</span>
	             <p><img src="{{asset('images/contact.png')}}" alt=""></p>
	        </li>
	        <li>
	            <input type="password" name="password" class="textbox2" placeholder="password">
	            @if ($errors->has('password'))
                                            <span class="error ">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
	            <p><img src="{{asset('images/lock.png')}}" alt=""></p>
	        </li>
         </ul>
       	 	<input type="submit" name="Sign In" value="Sign In"/>
			<div class="clear"></div>	
			<label class="checkbox"><input type="checkbox" name="checkbox" checked><i></i>Remember me</label>
		<div class="forgot">
			<a href="{{ route('password.request') }}">forgot password?</a>
		</div>	
		<div class="clear"></div>	
	</form>
	<!-- end-form -->
	<!-- start-account -->
	<div class="account">
	<h2><a href="{{route('register')}}">Don' have an account? Sign Up!</a></h2>
    <div class="span"><a href="{{route('social.auth',['provider'=>'facebook'])}}"><img src="images/facebook.png" alt=""/><i>Sign In with Facebook</i><div class="clear"></div></a></div>	
    <div class="span1"><a href="#"><img src="images/twitter.png" alt=""/><i>Sign In with Twitter</i><div class="clear"></div></a></div>
    <div class="span2"><a href="{{route('social.auth',['provider'=>'google'])}}"><img src="images/gplus.png" alt=""/><i>Sign In with Google+</i><div class="clear"></div></a></div>
	</div>	
	<!-- end-account -->
	<div class="clear"></div>	
	</div>
	<!-- end-contact-form -->
      </div> <!-- Main -->
    </div><!-- Wrapper -->


    <!-- Jquery and Js Plugins -->
    <script src="{{asset('assets-new/js/jquery-2.1.1.js')}}"></script>
    <script src="{{asset('assets-new/js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets-new/js/popper.min.js')}}"></script>
    <script src="{{asset('assets-new/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets-new/js/plugins.js')}}"></script>
    <script src="{{asset('assets-new/js/jquery.accordion.js')}}"></script>
    <script src="{{asset('assets-new/js/custom.js')}}"></script>
    
  </body>
</html>
