<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iofrm</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/iofrm-style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/iofrm-theme14.css')}}">
</head>
<body>
    <div class="form-body" class="container-fluid">
        <div class="row">
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <div class="website-logo-inside">
                             <a class="navbar-brand" href="{{route('homepage')}}"><img src="{{asset('assets-new/images/logo.png')}}" width="50" alt="Feature"></a>
                        </div>
                        <!--<h3>Welcome to Secure Payments.</h3>-->
                        <!--<p>Join the community of thousands of people who trust and use GreenTrust for their online payments.</p>-->
                        <div class="page-links">
                            <a href="{{route('login')}}">Login</a><a href="{{route('register')}}" class="active">Register</a>
                        </div>
                       
                                <form method="POST" action="{{ route('register') }}" >
                                         {{csrf_field()}}
                            <input class="form-control @if($errors->has('name')) error  @endif" type="text" name="name" placeholder="Full Name" required>
                             @if ($errors->has('name'))
                                                <span class="error ">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                            <input class="form-control" type="text" name="username" placeholder="Username" required>
                               @if ($errors->has('username'))
                                                <span class="error">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                            @endif
                        	<input class="form-control" type="email" name="email" placeholder="Enter Your Email" required>
                        	 @if ($errors->has('email'))
                                                <span class="error ">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
							<input class="form-control" type="text" name="phone" placeholder="Phone Number" required>
							  @if ($errors->has('phone'))
                                                <span class="error ">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                            @endif
							
                            <input class="form-control" type="password" name="password" placeholder="Password" required>
                             @if ($errors->has('password'))
                                                <span class="error ">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
							<input class="form-control" type="password" name="password_confirmation" placeholder="Re-enter Password" required>
							
                            <div class="form-button">
                                <button id="submit" type="submit" class="ibtn">Register</button>
                            </div>
                        </form>
                        <div class="other-links">
                            <span>Or register with</span><a href="{{route('social.auth',['provider'=>'facebook'])}}"><i class="fab fa-facebook-f"></i></a><a href="{{route('social.auth',['provider'=>'google'])}}"><i class="fab fa-google"></i></a><a href="#"><i class="fab fa-twitter"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</body>
</html>