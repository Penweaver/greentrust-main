<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GreenTrust Login</title>
     <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/iofrm-style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/iofrm-theme14.css')}}">

</head>
<body>
    <div class="form-body" class="container-fluid" >
        <div class="row">
            <div class="form-holder" >
                <div class="form-content">
                    <div class="form-items" >
                        <div class="website-logo-inside">
                            <a href="{{route('homepage')}}">
                                <!--<div class="logo">-->
                                     <a class="navbar-brand" href="{{route('homepage')}}"><img src="{{asset('assets-new/images/logo.png')}}" width="50" alt="Feature"></a>
         <!--                           <img class="logo-size" src="images/logo-light.svg" alt="">-->
									<!--<p> GrenTrust Payment Solution</p>-->
                                <!--</div>-->
                            </a>
                        </div>
                        <!--<h3>The Best Secured Payments Platform.</h3>-->
                        <!--<p>Join the community of thousands of people who trust and use GreenTrust for their online payments.</p>-->
                        <div class="page-links">
                            <a href="{{route('login')}}" class="active">Login</a><a href="{{route('register')}}">Register</a>
                        </div>
                       	<form  action="{{ route('login') }}" method="post" name="contact_form">
                       	    	     {{csrf_field()}}
                            <input class="form-control" type="text" name="username" placeholder="UserName" required>
                               @if ($errors->has('username'))
                                            <span class="error ">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                        @endif
                            <input class="form-control" type="password" name="password" placeholder="Password" required>
                              @if ($errors->has('password'))
                                            <span class="error ">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                            <div class="form-button">
                                <button id="submit" type="submit" class="ibtn">Login</button>
                            </div>
                        </form>
                        <div class="other-links">
                            <span>Or login with</span>
							<a href="{{route('social.auth',['provider'=>'facebook'])}}"><i class="fab fa-facebook-f"></i></a>
							<a href="{{route('social.auth',['provider'=>'google'])}}"><i class="fab fa-google"></i></a>
							<a href="#"><i class="fab fa-twitter"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</body>
</html>