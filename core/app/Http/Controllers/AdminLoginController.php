<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\GeneralSettings;
use SocialAuth;
use Socialite;
class AdminLoginController extends Controller
{


	public function __construct(){
		$Gset = GeneralSettings::first();
		$this->sitename = $Gset->sitename;
	}


	public function index(){

		if(Auth::guard('admin')->check()){
			return redirect()->route('admin.dashboard');
		}
		
		$data['page_title'] = "Admin";
		return view('admin.loginform', $data);
	}

	public function authenticate(Request $request){
		if (Auth::guard('admin')->attempt([
			'username' => $request->username,
			'password' => $request->password,
		])) {
			return "ok";
		}
		return "The Combination of Username and Password is Wrong!";
	}




    public function facebook_auth($provider){

        return SocialAuth::authorize($provider);
    }

    public function callback_auth($provider){
      
        SocialAuth::login($provider, function($user, $details){
            $user->username = str_slug($details->full_name);
            $user->name = $details->full_name;
            $user->email = $details->email;
            $user->phone_verify = '1';
            $user->email_verify = ($details->email != "") ? 1 : 0;
        });
         
        $user = Auth::user();
        
        return	redirect()->route('home');//dd($user);
        
    }


}
