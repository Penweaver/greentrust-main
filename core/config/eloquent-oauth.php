<?php

use App\User;

return [
    'model' => User::class,
    'table' => 'oauth_identities',
    'providers' => [
        'facebook' => [
            'client_id' => '1444234782373281',
            'client_secret' => 'be9be9794632b94990224193d8b5e375',
            'redirect_uri' => 'https://www.greentrust.com.ng/facebook/redirect/',
            'scope' => ['public_profile'],
        ],
        'google' => [
            // 'client_id' => '545484496371-21eegjccbueirb7slnkl5r4hl21vmtg1.apps.googleusercontent.com',
            // 'client_secret' => 'Xv-Rq_GsU3wiE6wdD4qEKf7k',
              'client_id' => '287731893886-rtfaoklgn27h7b93b5e75jq85qs52lab.apps.googleusercontent.com',
            'client_secret' => 'ftFkKopSpjVcac_wXbPDyi-a',
            'redirect_uri' => 'https://www.greentrust.com.ng/google/redirect/',
            'scope' => [],
        ],
        'github' => [
            'client_id' => '12345678',
            'client_secret' => 'y0ur53cr374ppk3y',
            'redirect_uri' => 'https://www.greentrust.com.ng/github/redirect',
            'scope' => [],
        ],
        'linkedin' => [
            'client_id' => '12345678',
            'client_secret' => 'y0ur53cr374ppk3y',
            'redirect_uri' => 'https://www.greentrust.com.ng/linkedin/redirect',
            'scope' => [],
        ],
        'instagram' => [
            'client_id' => '5e8a67440ea344ad9824d7d2cc4820f6',
            'client_secret' => 'b8f4817c1c2b4fd88a1a1d708e8fc256',
            'redirect_uri' => 'https://www.greentrust.com.ng/',
            'scope' => [],
        ],
        'soundcloud' => [
            'client_id' => '12345678',
            'client_secret' => 'y0ur53cr374ppk3y',
            'redirect_uri' => 'https://example.com/your/soundcloud/redirect',
            'scope' => [],
        ],
    ],
];
